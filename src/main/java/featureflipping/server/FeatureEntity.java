package featureflipping.server;


import javax.persistence.Convert;
import javax.persistence.Converter;
import javax.persistence.Entity;
import javax.persistence.Id;

import featureflipping.domain.RoleExpression;

@Entity
public class FeatureEntity {

	@Id
	private String name;
	
	@Convert(converter = RoleExpressionConverter.class)
	private RoleExpression expression;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public RoleExpression getExpression() {
		return expression;
	}

	public void setExpression(RoleExpression expression) {
		this.expression = expression;
	}
	
	
}
