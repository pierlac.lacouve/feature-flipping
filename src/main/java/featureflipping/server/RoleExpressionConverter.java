package featureflipping.server;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import featureflipping.domain.RoleExpression;


public class  RoleExpressionConverter implements AttributeConverter<RoleExpression, String> {

	private final ObjectMapper mapper = new ObjectMapper();

	@Override
	public String convertToDatabaseColumn(RoleExpression attribute) {
		try {
			return (attribute != null) ? mapper.writeValueAsString(attribute) : null;
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public RoleExpression convertToEntityAttribute(String dbData) {
		try {
			return (dbData != null) ? mapper.readValue(dbData, RoleExpression.class) : null;
		} catch (JsonProcessingException e) {
			throw new RuntimeException(e);
		}
	}

}
