package featureflipping.server;

import featureflipping.domain.Feature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/features")
public class FeatureController {

    @Autowired
    FeatureService service;

    @PostMapping()
    public ResponseEntity<Void> createFeature(@RequestBody Feature feature){
        if (feature == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        Feature savedFeature = service.save(feature);
        return ResponseEntity.created(URI.create("/features/"+savedFeature.getName())).build();

    }

    @GetMapping("/{id}")
    public ResponseEntity<Feature> getFeature(@PathVariable("id") String id){
        if(StringUtils.hasText(id)){
            Feature feature = service.get(id);
            if(feature==null){
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<>(feature, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<Void> updateFeature(@PathVariable("id") String id,@RequestBody Feature feature){
        if(!StringUtils.hasText(id) || feature == null || !id.equals(feature.getName())){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        if(service.get(id)==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        service.save(feature);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFeature(@PathVariable("id") String id){
        if(!StringUtils.hasText(id)){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }
}
