package featureflipping.server;

import java.util.function.Predicate;

import featureflipping.domain.*;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public class RoleExpressionParser {
	public RoleExpression parse(String expression) {
		Assert.notNull(expression, "expression cannot be null");

		if (!StringUtils.hasText(expression) ) {
			return new RoleExpression() {
				
				@Override
				public String toStringRepresentation() {
					return "";
				}
				
				@Override
				public Predicate<Roles> toPredicate() {
					return roles -> true;
				}
			};
		}
		
		return innerParse(expression);
	}

	private RoleExpression innerParse(String expression) {
		if ( expression.contains("|") ) {
			String[] parts = expression.split("\\|", 2);
			return new OrRoleOperation(innerParse(parts[0]), innerParse(parts[1]));
		}

		if ( expression.contains("&") ) {
			String[] parts = expression.split("&", 2);
			return new AndRoleOperation(innerParse(parts[0]), innerParse(parts[1]));
		}

		return newRoleTerm(expression);
	}
	
	private RoleTerm newRoleTerm(String roleName) {
		try {
			if (!StringUtils.hasText(roleName)) {
				throw new ParseException("Expected role or operand");
			}
			return new RoleTerm(new Role(roleName.trim()));
		} catch (IllegalArgumentException e) {
			throw new ParseException("Failed to create role term : " + e.getMessage(), e);
		}
	}

	public static class ParseException extends RuntimeException {
		
		private static final long serialVersionUID = -1332069539480865763L;

		public ParseException(String message, Throwable cause) {
			super(message, cause);
		}
		
		public ParseException(String message) {
			super(message);
		}
		
	}
}
