package featureflipping.server;

import org.springframework.data.repository.CrudRepository;

public interface FeatureRepository
	extends CrudRepository<FeatureEntity, String> {

}
