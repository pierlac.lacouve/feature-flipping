package featureflipping.server;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import featureflipping.domain.Feature;

@Service
public class FeatureService {

	@Autowired
	FeatureRepository repository;

	public Feature get(String name) {
		Assert.hasText(name, "name cannot be nul/empty/blank");

		Optional<FeatureEntity> optional = repository.findById(name);

		if (optional.isPresent()) {
			FeatureEntity entity = optional.get();
			return new Feature(entity.getName(), entity.getExpression());
		}

		return null;
	}

	public Feature save(Feature feature) {
		Assert.notNull(feature, "feature cannot be null");

		FeatureEntity entity = new FeatureEntity();
		entity.setName(feature.getName());
		entity.setExpression(feature.getExpression());
		FeatureEntity savedEntity = repository.save(entity);

		return new Feature(savedEntity.getName(), savedEntity.getExpression());
	}

	public void delete(String name) {
		if (StringUtils.hasText(name))
			try {
				repository.deleteById(name);
			} catch (EmptyResultDataAccessException e) {
				// ignore it !
			}
	}

}
