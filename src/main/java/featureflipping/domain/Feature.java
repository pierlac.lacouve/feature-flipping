package featureflipping.domain;

import java.util.Objects;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Feature {

	@JsonProperty("name")
	private final String name;
	
	@JsonProperty("expression")
	private RoleExpression expression;

	public Feature(String name) {
		Assert.hasText(name, "name cannot be null/empty/blank");
		this.name = name;

	}

	@JsonCreator
	public Feature(@JsonProperty("name") String name, @JsonProperty("expression") RoleExpression expression) {
		Assert.hasText(name, "name cannot be null/empty/blank");
		this.name = name;
		this.expression = expression;

	}

	public String getName() {
		return name;
	}

	public RoleExpression getExpression() {
		return expression;
	}

	public void setExpression(RoleExpression expression) {
		this.expression = expression;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Feature other = (Feature) obj;
		return Objects.equals(name, other.name);
	}

}
