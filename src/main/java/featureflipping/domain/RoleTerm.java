package featureflipping.domain;

import java.util.function.Predicate;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RoleTerm implements RoleExpression {

	@JsonProperty("role")
	private final Role role;

	@JsonCreator
	public RoleTerm(@JsonProperty("role") Role role) {
		Assert.notNull(role, "role cannot be null");
		this.role = role;
	}
	
	@Override
	public Predicate<Roles> toPredicate() {
		return roles -> roles.contains(role);
	}

	@Override
	public String toStringRepresentation() {
		return role.getName();
	}

	
}
