package featureflipping.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class RoleExpressionConverter implements AttributeConverter<RoleExpression,String> {
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public String convertToDatabaseColumn(RoleExpression roleExpression) {
        if(roleExpression == null) return null;
        try {
            return objectMapper.writeValueAsString(roleExpression);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public RoleExpression convertToEntityAttribute(String s) {
        if( s == null ) return null;
        try {
            return objectMapper.readValue(s,RoleExpression.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
