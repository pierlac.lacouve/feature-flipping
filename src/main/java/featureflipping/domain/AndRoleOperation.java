package featureflipping.domain;

import java.util.function.Predicate;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AndRoleOperation extends RoleOperation {

	@JsonCreator
	public AndRoleOperation(@JsonProperty("left") RoleExpression left, @JsonProperty("right") RoleExpression right) {
		super(left, right);
	}

	@Override
	public Predicate<Roles> toPredicate() {
		return left.toPredicate().and(right.toPredicate());
	}

	@Override
	public String toStringRepresentation() {
		return "(" + left.toStringRepresentation() + " & " + right.toStringRepresentation() + ")";
	}

}
