package featureflipping.domain;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonSubTypes({
	@Type(value = AndRoleOperation.class, name = "and"),
	@Type(value = OrRoleOperation.class, name = "or")
})
public abstract class RoleOperation implements RoleExpression {

	@JsonProperty("left") 
	protected RoleExpression left;
	
	@JsonProperty("right") 
	protected RoleExpression right;
	
	protected RoleOperation(RoleExpression left, RoleExpression right) {
		Assert.notNull(left, "left cannot be null");
		Assert.notNull(right, "right cannot be null");
		this.left = left;
		this.right = right;
	}
	
}
