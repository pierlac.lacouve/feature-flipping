package featureflipping.domain;

import java.util.function.Predicate;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@JsonTypeInfo(
  use = JsonTypeInfo.Id.NAME, 
  include = JsonTypeInfo.As.PROPERTY, 
  property = "@type")
@JsonSubTypes({
	@Type(value = RoleTerm.class, name = "term"),
	@Type(value = RoleOperation.class)
})
public interface RoleExpression {

	Predicate<Roles> toPredicate();
	
	String toStringRepresentation();
}
