package featureflipping.domain;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

public class Roles {
	
	private final Set<Role> roles;
		
	private Roles(Set<Role> roles) {
		this.roles = roles;
	}

	public boolean contains(Role role) {
		return roles.contains(role);
	}

	public static Roles of(Role ...roles) {
		Assert.notNull(roles, "roles cannot be null");
		
		return new Roles(new HashSet<Role>(Arrays.asList(roles)));
	}

	public static Roles of(String ... roleNames) {
		Assert.notNull(roleNames, "roleNames cannot be null");
		
		return new Roles(Arrays.stream(roleNames)
				.filter(StringUtils::hasLength)
				.map(Role::new)
				.collect(Collectors.toSet()));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
