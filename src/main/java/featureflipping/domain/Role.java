package featureflipping.domain;

import java.util.Objects;
import java.util.regex.Pattern;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Role {

	@JsonProperty("name")
	private final String name;

	@JsonCreator
	public Role(@JsonProperty("name") String name) {
		Assert.hasText(name, "name cannot be null, empty or blank");
		Assert.isTrue(Pattern.matches("[a-zA-Z][a-zA-Z0-9_-]*", name), "name must start with a letter and contain only letters, digits, - or _");

		this.name = name;
	}

	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Role other = (Role) obj;
		return Objects.equals(name, other.name);
	}
	
	

	
	
	
	
	
	
	
	
	
	
	
	
}
