package featureflipping.server;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import featureflipping.domain.Role;
import featureflipping.domain.RoleExpression;
import featureflipping.domain.RoleTerm;

class RoleExpressionConverterTest {

	@Test
	void mustSerializeNullExpressionToNullString() {
		assertThat(getConverter().convertToDatabaseColumn(null)).isNull();
	}

	@Test
	void mustSerializeExpressionToJsonString() {
		RoleExpression re = new RoleTerm(new Role("test"));
		String json = getConverter().convertToDatabaseColumn(re); 
		assertThat(json).isNotNull();
		assertThat(json).isEqualTo("{\"@type\":\"term\",\"role\":{\"name\":\"test\"}}");
	}

	@Test
	void mustDeserializeNullExpressionFromNullString() {
		assertThat(getConverter().convertToEntityAttribute(null)).isNull();
	}

	@Test
	void mustDeserializeExpressionFromJsonString() {
		String json = "{\"@type\":\"term\",\"role\":{\"name\":\"test\"}}";
		RoleExpression re = getConverter().convertToEntityAttribute(json);
		assertThat(re).isNotNull();		
		assertThat(re.toStringRepresentation()).isEqualTo("test");
	}

	RoleExpressionConverter getConverter() {
		return new RoleExpressionConverter();
	}
		
}
