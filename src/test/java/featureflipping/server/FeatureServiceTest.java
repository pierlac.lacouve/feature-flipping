package featureflipping.server;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import featureflipping.domain.Feature;
import featureflipping.domain.Role;
import featureflipping.domain.RoleTerm;

@ExtendWith(MockitoExtension.class)
class FeatureServiceTest {

	@Mock
	private FeatureRepository repository;

	@InjectMocks
	FeatureService service;
	
	@Test
	void cannotGetFeatureWithNullName() {
		assertThrows(IllegalArgumentException.class, () -> {
			service.get(null);
		});
		verify(repository, never()).findById(any());
	}

	@Test
	void cannotGetFeatureWithEmptyName() {
		assertThrows(IllegalArgumentException.class, () -> {
			service.get("");
		});
		verify(repository, never()).findById(any());
	}

	@Test
	void cannotSaveNullFeature() {
		assertThrows(IllegalArgumentException.class, () -> {
			service.save(null);
		});
		verify(repository, never()).save(any());
	}
	
	@Test
	void mustRetrieveExitingFeatureFromRepository() {
		FeatureEntity mockEntity = new FeatureEntity();
		mockEntity.setName("myFeature");
		mockEntity.setExpression(new RoleTerm(new Role("toto")));
		
		when(repository.findById(any())).thenReturn(Optional.of(mockEntity));

		Feature feature = service.get("myFeature");

		verify(repository).findById(eq("myFeature"));
		assertThat(feature).isNotNull();
		assertThat(feature.getName()).isEqualTo("myFeature");
		assertThat(feature.getExpression()).isNotNull();
		assertThat(feature.getExpression().toStringRepresentation()).isEqualTo("toto");
	}
	
	@Test
	void mustReturnNullForNonExistingFeatureInRepository() {
		when(repository.findById(any())).thenReturn(Optional.empty());

		Feature feature = service.get("myMissingFeature");
		
		verify(repository).findById(eq("myMissingFeature"));
		assertThat(feature).isNull();
	}
	
	
	@Test
	void mustDeleteFeatureFromRepository() {
		service.delete("myMissingFeature");
		
		verify(repository).deleteById(eq("myMissingFeature"));
	}
	
	@Test
	void mustNotDeleteNullFeatureNameFromRepository() {
		service.delete(null);
		
		verify(repository, never()).deleteById(any());
	}
	
	@Test
	void mustSaveFeatureIntoRepository() {
		Feature feature = new Feature("new feature", new RoleTerm(new Role("test")));
		FeatureEntity savedEntity = new FeatureEntity();
		savedEntity.setName("saved feature");
		savedEntity.setExpression(new RoleTerm(new Role("AZERTY")));
		
		when(repository.save(any())).thenReturn(savedEntity);
		
		ArgumentCaptor<FeatureEntity> captor = ArgumentCaptor.forClass(FeatureEntity.class);
		
		Feature savedFeature = service.save(feature);
		
		
		assertThat(savedFeature).isNotNull();
		assertThat(savedFeature.getName()).isEqualTo("saved feature");
		assertThat(savedFeature.getExpression()).isNotNull();
		assertThat(savedFeature.getExpression().toStringRepresentation()).isEqualTo("AZERTY");
		
		verify(repository).save(captor.capture());
		assertThat(captor.getValue()).isNotNull();
		assertThat(captor.getValue().getName()).isEqualTo("new feature");
		assertThat(captor.getValue().getExpression()).isNotNull();
		assertThat(captor.getValue().getExpression().toStringRepresentation()).isEqualTo("test");		
	
	}

}