package featureflipping.server;

import static org.mockito.Mockito.*;
import static org.assertj.core.api.Assertions.assertThat;
import featureflipping.domain.Feature;
import featureflipping.domain.Role;
import featureflipping.domain.RoleTerm;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;


@ExtendWith(MockitoExtension.class)
class FeatureControllerTest {

    @Mock
    private FeatureService service;

    @InjectMocks
    FeatureController controller;




    @Test
    void cannotCreateFeatureWithNullArgument(){
        ResponseEntity<Void> responseEntity = controller.createFeature(null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).save(any());
    }

    @Test
    void mustCreateValidFeature(){
        Feature mockedFeature = new Feature("mockedFeature",new RoleTerm(new Role("admin")));
        Feature feature = new Feature("myFeature",new RoleTerm(new Role("dev")));

        when(service.save(any())).thenReturn(mockedFeature);
        ArgumentCaptor<Feature> captor = ArgumentCaptor.forClass(Feature.class);

        ResponseEntity<Void> responseEntity = controller.createFeature(feature);

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(201);
        assertThat(responseEntity.getHeaders().getLocation().toString()).endsWith("/features/mockedFeature");

        verify(service).save(captor.capture());
        assertThat(captor.getValue()).isNotNull();
        assertThat(captor.getValue().getName()).isEqualTo("myFeature");
        assertThat(captor.getValue().getExpression()).isNotNull();
        assertThat(captor.getValue().getExpression().toStringRepresentation()).isEqualTo("dev");
    }





    @Test
    void cannotGetFeatureWithNullName() {
        ResponseEntity<Feature> responseEntity = controller.getFeature(null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).get(any());
    }

    @Test
    void cannotGetFeatureWithEmptyName() {
        ResponseEntity<Feature> responseEntity = controller.getFeature("");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).get(any());
    }

    @Test
    void cannotGetFeatureWithBlankName() {
        ResponseEntity<Feature> responseEntity = controller.getFeature("          ");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).get(any());
    }

    @Test
    void cannotGetNonExistingFeature() {
        ResponseEntity<Feature> responseEntity = controller.getFeature("nonExistingFeature");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);
        verify(service).get("nonExistingFeature");
    }

    @Test
    void mustGetValidFeature(){
        Feature mockedFeature = new Feature("mockedFeature",new RoleTerm(new Role("admin")));

        when(service.get(anyString())).thenReturn(mockedFeature);

        ResponseEntity<Feature> responseEntity = controller.getFeature("existingFeature");


        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        assertThat(responseEntity.getBody().hashCode()).isEqualTo(mockedFeature.hashCode());
        verify(service).get("existingFeature");
    }





    @Test
    void cannotUpdateFeatureWithNullId() {
        Feature feature = new Feature("myFeature",new RoleTerm(new Role("dev")));

        ResponseEntity<Void> responseEntity = controller.updateFeature(null,feature);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);

        verify(service,never()).save(any());
    }

    @Test
    void cannotUpdateFeatureWithEmptyId() {
        Feature feature = new Feature("myFeature",new RoleTerm(new Role("dev")));

        ResponseEntity<Void> responseEntity = controller.updateFeature("",feature);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).save(any());
    }

    @Test
    void cannotUpdateFeatureWithBlankId() {
        Feature feature = new Feature("myFeature",new RoleTerm(new Role("dev")));

        ResponseEntity<Void> responseEntity = controller.updateFeature("          ",feature);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).save(any());
    }

    @Test
    void cannotUpdateFeatureWithNullFeature() {
        ResponseEntity<Void> responseEntity = controller.updateFeature("myFeature",null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).save(any());
    }

    @Test
    void cannotUpdateDifferentFeatureNameFromId() {
        Feature feature = new Feature("myNewFeature",new RoleTerm(new Role("dev")));
        ResponseEntity<Void> responseEntity = controller.updateFeature("myFeature",feature);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).save(any());
    }

    @Test
    void cannotUpdateNonExistingFeature() {
        Feature feature = new Feature("nonExistingFeature",new RoleTerm(new Role("dev")));

        ArgumentCaptor<String> captorGet = ArgumentCaptor.forClass(String.class);

        ResponseEntity<Void> responseEntity = controller.updateFeature("nonExistingFeature",feature);

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(404);

        verify(service).get(captorGet.capture());
        assertThat(captorGet.getValue()).isNotNull();
        assertThat(captorGet.getValue()).isEqualTo("nonExistingFeature");

        verify(service,never()).save(any());
    }

    @Test
    void mustUpdateValidFeature() {
        Feature mockedFeature = new Feature("myUpdatedFeature",new RoleTerm(new Role("dev")));
        Feature feature = new Feature("myUpdatedFeature",new RoleTerm(new Role("dev")));

        ArgumentCaptor<String> captorGet = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Feature> captorSave = ArgumentCaptor.forClass(Feature.class);

        when(service.get(any())).thenReturn(mockedFeature);

        ResponseEntity<Void> responseEntity = controller.updateFeature("myUpdatedFeature",feature);

        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);

        verify(service).save(captorSave.capture());
        assertThat(captorSave.getValue()).isNotNull();
        assertThat(captorSave.getValue().getName()).isEqualTo("myUpdatedFeature");
        assertThat(captorSave.getValue().getExpression()).isNotNull();
        assertThat(captorSave.getValue().getExpression().toStringRepresentation()).isEqualTo("dev");

        verify(service).get(captorGet.capture());
        assertThat(captorGet.getValue()).isNotNull();
        assertThat(captorGet.getValue()).isEqualTo("myUpdatedFeature");
    }





    @Test
    void cannotDeleteFeatureWithNullId() {
        ResponseEntity<Void> responseEntity = controller.deleteFeature(null);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).delete(any());
    }

    @Test
    void cannotDeleteFeatureWithEmptyId() {
        ResponseEntity<Void> responseEntity = controller.deleteFeature("");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).delete(any());
    }

    @Test
    void cannotDeleteFeatureWithBlankId() {
        ResponseEntity<Void> responseEntity = controller.deleteFeature("          ");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(400);
        verify(service,never()).delete(any());
    }

    @Test
    void mustDeleteValidFeature(){
        ResponseEntity<Void> responseEntity = controller.deleteFeature("deletableFeature");
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
        verify(service).delete("deletableFeature");
    }
}