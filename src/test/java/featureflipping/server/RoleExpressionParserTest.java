package featureflipping.server;

import static org.assertj.core.api.Assertions.assertThat;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import featureflipping.domain.AndRoleOperation;
import featureflipping.domain.OrRoleOperation;
import featureflipping.domain.RoleExpression;
import featureflipping.domain.RoleTerm;
import featureflipping.domain.Roles;
import featureflipping.server.RoleExpressionParser.ParseException;

class RoleExpressionParserTest {

	@Test
	public void mustNotParseNull() {
		mustNotParse(null, IllegalArgumentException.class);
	}

	@Test
	public void mustReturnTautologyForEmptyString() {
		RoleExpression re = mustParse("", RoleExpression.class, "");
		assertThat(re.toPredicate().test(Roles.of(""))).isTrue();

		re = mustParse(" ", RoleExpression.class, "");
		assertThat(re.toPredicate().test(Roles.of(""))).isTrue();
	}

	@Test
	public void mustParseSimpleTerm() {
		mustParse("dev", RoleTerm.class, "dev");
	}

	@Test
	public void mustParseSimpleAnd() {
		mustParse("dev & admin", AndRoleOperation.class, "(dev & admin)");
	}

	@Test
	public void mustParseSimpleOr() {
		mustParse("dev | admin", OrRoleOperation.class, "(dev | admin)");
	}

	@Test
	public void mustNotParseAndWithMissingOperands() {
		mustNotParse("&");
		mustNotParse("dev &");
		mustNotParse("& admin");
	}

	@Test
	public void mustNotParseOrWithMissingOperands() {
		mustNotParse("|");
		mustNotParse("dev |");
		mustNotParse("| admin");
	}

	@Test
	public void mustNotParseWithMissingOperator() {
		mustNotParse("dev admin");
	}

	@Test
	public void mustParseMultipleAnd() {
		mustParse("dev & admin & rh", AndRoleOperation.class, "(dev & (admin & rh))","((dev & admin) & rh)");
	}

	@Test
	public void mustParseMultipleOr() {
		mustParse("dev | admin | rh", OrRoleOperation.class, "(dev | (admin | rh))","((dev | admin) | rh)");
	}

	@Test
	public void mustPrioritizeAndOverOr() {
		mustParse("dev | admin & rh", OrRoleOperation.class, "(dev | (admin & rh))");
		mustParse("dev & admin | rh", OrRoleOperation.class, "((dev & admin) | rh)");
	}


	private void mustNotParse(String string) {
		mustNotParse(string, ParseException.class);
	}

	private void mustNotParse(String string, Class<? extends Exception> ex) {
		Assertions.assertThatThrownBy(() -> parse(string)).isInstanceOf(ex);
	}

	private RoleExpression mustParse(String string, String ... expectedRepresentations) {
		return mustParse(string, RoleExpression.class, expectedRepresentations);
	}

	private RoleExpression mustParse(String string, Class<? extends RoleExpression> cls,
			String ... expectedRepresentations) {
		assertThat(expectedRepresentations).isNotEmpty();
		RoleExpression re = parse(string);
		assertThat(re).isInstanceOf(cls);
		assertThat(re.toStringRepresentation()).isIn(expectedRepresentations);
		return re;
	}

	RoleExpression parse(String string) {
		return newParser().parse(string);
	}

	RoleExpressionParser newParser() {
		return new RoleExpressionParser(); 
	};
}
