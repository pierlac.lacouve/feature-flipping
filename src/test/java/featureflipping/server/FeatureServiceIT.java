package featureflipping.server;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import featureflipping.domain.Feature;
import featureflipping.domain.Role;
import featureflipping.domain.RoleTerm;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = Main.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FeatureServiceIT {

	@Autowired
	FeatureService service;
	@Autowired
	TestRestTemplate testRestTemplate;
	@Test
	void mustStart() {
		;
	}

	@Test
	void mustPersist() {
		
		// nothing at first
		assertThat(service.get("test")).isNull();
		
		// create feature
		Feature feature = new Feature("test",new RoleTerm(new Role("ROLE_TEST")));
		service.save(feature);
		
		// retrieve feature
		Feature gotFeature = service.get("test"); 
		assertThat(gotFeature).isNotNull();
		assertThat(gotFeature.getName()).isEqualTo("test");
		assertThat(gotFeature.getExpression()).isNotNull();
		assertThat(gotFeature.getExpression().toStringRepresentation()).isEqualTo("ROLE_TEST");
		
		// update feature
		feature = new Feature("test",new RoleTerm(new Role("ROLE_ADMIN")));
		service.save(feature);
		gotFeature = service.get("test");
		assertThat(gotFeature).isNotNull();
		assertThat(gotFeature.getName()).isEqualTo("test");
		assertThat(gotFeature.getExpression()).isNotNull();
		assertThat(gotFeature.getExpression().toStringRepresentation()).isEqualTo("ROLE_ADMIN");
		
		// delete feature
		service.delete("test");
		
		// nothing left
		assertThat(service.get("test")).isNull();
		
		// delete inexisting feature
		service.delete("test");
	}

	@Test
	void mustPersistEnd2End(){
		// nothing at first
		ResponseEntity<Feature> responseEntity = testRestTemplate.getForEntity("/features/myFeature",Feature.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

		// create feature
		Feature feature = new Feature("myFeature",new RoleTerm(new Role("ROLE_TEST")));
		ResponseEntity<Void> responseEntityVoid = testRestTemplate.postForEntity("/features",feature,Void.class);
		assertThat(responseEntityVoid.getStatusCode()).isEqualTo(HttpStatus.CREATED);
		assertThat(responseEntityVoid.getHeaders().getLocation().toString()).endsWith("/features/myFeature");


		// retrieve feature
		responseEntity = testRestTemplate.getForEntity("/features/myFeature",Feature.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

		assertThat(responseEntity.hasBody()).isTrue();
		assertThat(responseEntity.getBody().getName()).isEqualTo("myFeature");
		assertThat(responseEntity.getBody().getExpression()).isNotNull();
		assertThat(responseEntity.getBody().getExpression().toStringRepresentation()).isEqualTo("ROLE_TEST");

		// update feature
		feature = new Feature("myFeature",new RoleTerm(new Role("ROLE_ADMIN")));
		testRestTemplate.put("/features/myFeature",feature);

		responseEntity = testRestTemplate.getForEntity("/features/myFeature",Feature.class);
		assertThat(responseEntity.hasBody()).isTrue();
		assertThat(responseEntity.getBody().getName()).isEqualTo("myFeature");
		assertThat(responseEntity.getBody().getExpression()).isNotNull();
		assertThat(responseEntity.getBody().getExpression().toStringRepresentation()).isEqualTo("ROLE_ADMIN");

		// delete feature
		testRestTemplate.delete("/features/myFeature");

		// nothing left
		responseEntity = testRestTemplate.getForEntity("/features/myFeature",Feature.class);
		assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

		// delete inexisting feature
		testRestTemplate.delete("/features/myFeature");
	}
}
