package featureflipping.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


class AndRoleOperationTest {

	@Test
	void mustReturnCorrectStringRepresentation() {
		assertThat(newAndRoleOperation("dev", "ops")
				.toStringRepresentation()).isEqualTo("(dev & ops)");
	}
	
	@Test
	void mustReturnFalsyPredicateWhenBothOperandsAreFalsy() {
		assertThat(newAndRoleOperation("dev", "ops")
				.toPredicate().test(Roles.of(""))).isFalse();
	}

	@Test
	void mustReturnFalsyPredicateWhenLeftOperandsIsFalsy() {
		assertThat(newAndRoleOperation("dev", "ops")
				.toPredicate().test(Roles.of("ops"))).isFalse();
	}

	@Test
	void mustReturnFalsyPredicateWhenRightOperandsIsFalsy() {
		assertThat(newAndRoleOperation("dev", "ops")
				.toPredicate().test(Roles.of("dev"))).isFalse();
	}

	@Test
	void mustReturnTruthyPredicateWhenBothOperandsAreTruthy() {
		assertThat(newAndRoleOperation("dev", "ops")
				.toPredicate().test(Roles.of("ops","dev"))).isTrue();
	}

	private AndRoleOperation newAndRoleOperation(String left, String right) {
		return new AndRoleOperation(new RoleTerm(new Role(left)), new RoleTerm(new Role(right)));
	}
	
	
	
	static String JSON = "{\"@type\":\"and\",\"left\":{\"@type\":\"term\",\"role\":{\"name\":\"dev\"}},\"right\":{\"@type\":\"term\",\"role\":{\"name\":\"admin\"}}}";
	
	@Test
	void mustSerialize() throws JsonProcessingException  {
		RoleExpression role = newAndRoleOperation("dev", "admin");
		ObjectMapper mapper = new ObjectMapper();
		String actualJson = mapper.writeValueAsString(role);
		assertThat(actualJson).isEqualTo(JSON);
	}
	
	
	@Test
	void mustDeserialize() throws JsonMappingException, JsonProcessingException {
		RoleExpression expected = newAndRoleOperation("dev", "admin");
		ObjectMapper mapper = new ObjectMapper();
		RoleExpression actual = mapper.readValue(JSON, RoleExpression.class);
		assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
	}
	
	
}
