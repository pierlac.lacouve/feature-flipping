package featureflipping.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class RoleTest {

	@Test
	void cannotBuildWithNullName() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Role(null);
		});
	}
	
	@Test
	void cannotBuildWithEmptyName() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Role("");
		});
	}
	
	@Test
	void cannotBuildWithBlankName() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Role("    ");
		});
	}

	@Test
	void cannotBuildWithNameStartingWithDigit() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Role("1ROLE");
		});
	}
	
	@Test
	void cannotBuildWithNameContainingSpaces() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Role("UN role");
		});
	}

	@Test
	void mustReturnItsName() {
		assertThat(new Role("admin").getName()).isEqualTo("admin");
	}
	
	@Test
	void mustNotBeEqualToNull() {
		assertThat(new Role("admin").equals(null)).isFalse();
	}
	
	@Test
	void mustBeEqualToItself() {
		Role role = new Role("dev");
		assertThat(role.equals(role)).isTrue();
	}
	
	@Test
	void mustNotBeEqualToSomethingElse() {
		assertThat(new Role("dev").equals("dev")).isFalse();
	}
	
	@Test
	void mustBeEqualToAnotherRoleWithSameName() {
		assertThat(new Role("dev").equals(new Role("dev"))).isTrue();
	}

	@Test
	void mustNotBeEqualToAnotherRoleWithDifferentName() {
		assertThat(new Role("dev").equals(new Role("admin"))).isFalse();
	}
	
	@Test
	void mustHaveConsistentHashcode() {
		Role role = new Role("dev");
		assertThat(role.hashCode()).isEqualTo(role.hashCode());
	}
	
	@Test
	void mustHaveSameHashcodeForIdentical() {
		assertThat(new Role("dev").hashCode()).isEqualTo(new Role("dev").hashCode());
	}

	
	@Test
	void mustSerialize() throws JsonProcessingException  {
		Role role = new Role("dev");
		String expectedJson = "{\"name\":\"dev\"}";
		ObjectMapper mapper = new ObjectMapper();
		String actualJson = mapper.writeValueAsString(role);
		assertThat(actualJson).isEqualTo(expectedJson);
	}
	
	
	@Test
	void mustDeserialize() throws JsonMappingException, JsonProcessingException {
		String json = "{\"name\":\"admin\"}";
		Role expectedRole = new Role("admin");
		ObjectMapper mapper = new ObjectMapper();
		Role actualRole = mapper.readValue(json, Role.class);
		assertThat(actualRole).isEqualTo(expectedRole);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
