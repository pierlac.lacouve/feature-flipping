package featureflipping.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


class OrRoleOperationTest {

	@Test
	void mustReturnCorrectStringRepresentation() {
		assertThat(newOrRoleOperation("dev", "ops")
				.toStringRepresentation()).isEqualTo("(dev | ops)");
	}
	
	@Test
	void mustReturnFalsyPredicateWhenBothOperandsAreFalsy() {
		assertThat(newOrRoleOperation("dev", "ops")
				.toPredicate().test(Roles.of(""))).isFalse();
	}

	@Test
	void mustReturnTruthyPredicateWhenLeftOperandsIsTruthy() {
		assertThat(newOrRoleOperation("dev", "ops")
				.toPredicate().test(Roles.of("dev"))).isTrue();
	}

	@Test
	void mustReturnTruthyPredicateWhenRightOperandsIsTruthy() {
		assertThat(newOrRoleOperation("dev", "ops")
				.toPredicate().test(Roles.of("ops"))).isTrue();
	}

	@Test
	void mustReturnTruthyPredicateWhenBothOperandsAreTruthy() {
		assertThat(newOrRoleOperation("dev", "ops")
				.toPredicate().test(Roles.of("ops","dev"))).isTrue();
	}

	private OrRoleOperation newOrRoleOperation(String left, String right) {
		return new OrRoleOperation(new RoleTerm(new Role(left)), new RoleTerm(new Role(right)));
	}
	
	

	static String JSON = "{\"@type\":\"or\",\"left\":{\"@type\":\"term\",\"role\":{\"name\":\"dev\"}},\"right\":{\"@type\":\"term\",\"role\":{\"name\":\"admin\"}}}";
	
	@Test
	void mustSerialize() throws JsonProcessingException  {
		RoleExpression role = newOrRoleOperation("dev", "admin");
		ObjectMapper mapper = new ObjectMapper();
		String actualJson = mapper.writeValueAsString(role);
		assertThat(actualJson).isEqualTo(JSON);
	}
	
	
	@Test
	void mustDeserialize() throws JsonMappingException, JsonProcessingException {
		RoleExpression expected = newOrRoleOperation("dev", "admin");
		ObjectMapper mapper = new ObjectMapper();
		RoleExpression actual = mapper.readValue(JSON, RoleExpression.class);
		assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
	}
	
	
}
