package featureflipping.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

class RolesTest {

	@Test
	void cannotBuildWithNullRoleArray() {
		assertThrows(IllegalArgumentException.class, () -> {
			Roles.of((Role[]) null);
		});
	}
	
	@Test
	void shouldIgnoreNullRole() {
		Roles.of((Role) null);
	}

	@Test
	void cannotBuildWithNullStringArray() {
		assertThrows(IllegalArgumentException.class, () -> {
			Roles.of((String[]) null);
		});
	}

	
	@Test
	void shouldIgnoreNullRoleName() {
		Roles.of((String) null);
	}

	@Test
	void shouldIgnoreEmptyRoleName() {
		Roles.of("");
	}

	@Test
	void mustContainProvidedRole() {
		assertThat(Roles.of("admin").contains(new Role("admin"))).isTrue();
	}
	
	@Test
	void mustContainProvidedRole2() {
		assertThat(Roles.of("admin","dev").contains(new Role("dev"))).isTrue();
	}
	
	@Test
	void mustNotContainUnprovidedRole() {
		assertThat(Roles.of("admin","dev").contains(new Role("ops"))).isFalse();
	}
	
	@Test
	void mustNotContainNull() {
		assertThat(Roles.of("admin","dev").contains(null)).isFalse();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
