package featureflipping.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.function.Predicate;

import org.junit.jupiter.api.Test;

class RoleOperationTest {

	private class TestRoleOperation extends RoleOperation {

		protected TestRoleOperation(RoleExpression left, RoleExpression right) {
			super(left, right);
		}

		@Override
		public Predicate<Roles> toPredicate() {
			return null;
		}

		@Override
		public String toStringRepresentation() {
			return null;
		}
		
		public RoleExpression left() { return left; }
		
		public RoleExpression right() { return right; }
	}
	
	@Test
	void cannotBuildWithNullLeftOperand() {
		assertThrows(IllegalArgumentException.class, 
				() -> newTestRoleOperation(null, "dev")
		);
	}

	@Test
	void cannotBuildWithNullRightOperand() {
		assertThrows(IllegalArgumentException.class, 
				() -> newTestRoleOperation("dzev", null)
		);
	}

	@Test
	void cannotBuildWithNullOperands() {
		assertThrows(IllegalArgumentException.class, 
				() -> newTestRoleOperation(null, null)
		);
	}

	@Test
	void mustAssignLeftOperand() {
		RoleExpression operand = newTestRoleOperation("dev", "ops").left(); 
		assertThat(operand.toStringRepresentation()).isEqualTo("dev");
		assertThat(operand).isExactlyInstanceOf(RoleTerm.class);
	}
	
	@Test
	void mustAssignRightOperand() {
		RoleExpression operand = newTestRoleOperation("dev", "ops").right(); 
		assertThat(operand.toStringRepresentation()).isEqualTo("ops");
		assertThat(operand).isExactlyInstanceOf(RoleTerm.class);
	}
	
	private TestRoleOperation newTestRoleOperation(String left, String right) {
		return new TestRoleOperation(
				left != null ? new RoleTerm(new Role(left)) : null,
						right != null ? new RoleTerm(new Role(right)) : null);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
