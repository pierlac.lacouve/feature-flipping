package featureflipping.domain;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class RoleTermTest {

	@Test
	void cannotBuilWithNullRole() {
		assertThrows(IllegalArgumentException.class, () -> new RoleTerm(null));
	}
	
	@Test
	void mustReturnCorrectStringRepresentation() {
		assertThat(new RoleTerm(new Role("pouet"))
				.toStringRepresentation()).isEqualTo("pouet");
	}

	@Test
	void mustReturnTruthyPredicateWhenRoleInRoles() {
		assertThat(new RoleTerm(new Role("admin"))
				.toPredicate().test(Roles.of("dev","admin"))).isTrue();
	}
	

	@Test
	void mustReturnFalsyPredicateWhenRoleNotInRoles() {
		assertThat(new RoleTerm(new Role("admin"))
				.toPredicate().test(Roles.of("dev","ops"))).isFalse();
	}

	static String JSON ="{\"@type\":\"term\",\"role\":{\"name\":\"dev\"}}"; 
	
	@Test
	void mustSerialize() throws JsonProcessingException  {
		RoleTerm role = new RoleTerm(new Role("dev"));
		ObjectMapper mapper = new ObjectMapper();
		String actualJson = mapper.writeValueAsString(role);
		assertThat(actualJson).isEqualTo(JSON);
	}
	
	
	@Test
	void mustDeserialize() throws JsonMappingException, JsonProcessingException {
		RoleTerm expected = new RoleTerm(new Role("dev"));
		ObjectMapper mapper = new ObjectMapper();
		RoleTerm actual = mapper.readValue(JSON, RoleTerm.class);
		assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
	}
	
	
}
