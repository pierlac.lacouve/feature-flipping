package featureflipping.domain;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;


class RoleExpressionConverterTest {
    public static final String JSON = "{\"@type\":\"term\",\"role\":{\"name\":\"dev\"}}";

    @Test
    public void mustConvertToJSON(){
       assertThat(new RoleExpressionConverter().convertToDatabaseColumn(new RoleTerm(new Role("dev")))).isEqualTo(JSON);
    }

    @Test
    public void mustConvertFromJSON(){
       assertThat(new RoleExpressionConverter().convertToEntityAttribute(JSON).toStringRepresentation()).isEqualTo("dev");
    }

    @Test
    public void mustConvertNullToJSON(){
       assertThat(new RoleExpressionConverter().convertToDatabaseColumn(null)).isNull();
    }

    @Test
    public void mustConvertNullFromJSON(){
       assertThat(new RoleExpressionConverter().convertToEntityAttribute(null)).isNull();
    }

}
