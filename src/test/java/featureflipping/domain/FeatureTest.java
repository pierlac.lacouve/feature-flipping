package featureflipping.domain;

import static org.assertj.core.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

class FeatureTest {

	@Test
	void cannotBuildFeatureWithNullName() {
		assertThrows(IllegalArgumentException.class, () -> new Feature(null));
	}

	@Test
	void cannotBuildFeatureWithEmptyName() {
		assertThrows(IllegalArgumentException.class, () -> new Feature(""));
	}

	@Test
	void cannotBuildFeatureWithBlankName() {
		assertThrows(IllegalArgumentException.class, () -> new Feature("   "));
	}

	@Test
	void cannotBuildFeatureWithNullNameAndAnExpression() {
		assertThrows(IllegalArgumentException.class, () -> new Feature(null, new RoleTerm(new Role("dev"))));
	}

	@Test
	void cannotBuildFeatureWithEmptyNameAndAnExpression() {
		assertThrows(IllegalArgumentException.class, () -> new Feature("", new RoleTerm(new Role("dev"))));
	}

	@Test
	void cannotBuildFeatureWithBlankNameAndAnExpression() {
		assertThrows(IllegalArgumentException.class, () -> new Feature("   ", new RoleTerm(new Role("dev"))));
	}


	@Test
	void canBuildFeatureNullExpression() {
		new Feature("test", null);
	}
	
	@Test
	void mustReturnItsName() {
		assertThat(new Feature("test", null).getName()).isEqualTo("test");
	}
	
	@Test
	void mustReturnItsInitialExpression() {
		assertThat(new Feature("test", null).getExpression()).isNull();
		RoleExpression re = new Feature("test", new RoleTerm(new Role("dev"))).getExpression();
		assertThat(re).isExactlyInstanceOf(RoleTerm.class);
		assertThat(re.toStringRepresentation()).isEqualTo("dev");
	}
	
	@Test
	void mustReturnItsSetExpression() {
		Feature feature = new Feature("test"); 
		assertThat(feature.getExpression()).isNull();
		feature.setExpression(new RoleTerm(new Role("dev")));
		RoleExpression re = feature.getExpression();
		assertThat(re).isExactlyInstanceOf(RoleTerm.class);
		assertThat(re.toStringRepresentation()).isEqualTo("dev");
	}
	
	
	@Test
	void mustMustNotBeEqualToNull() {
		assertThat(new Feature("test").equals(null)).isFalse();
	}

	@Test
	void mustMustBeEqualToSelf() {
		Feature feature = new Feature("test"); 
		assertThat(feature.equals(feature)).isTrue();
	}

	@Test
	void mustMustNotBeEqualToSomethingElse() {
		assertThat(new Feature("test").equals("test")).isFalse();
	}

	@Test
	void mustMustBeEqualToAnotherFeatureWithSameName() {
		Feature feature = new Feature("test", new RoleTerm(new Role("dev")));
		assertThat(feature.equals(new Feature("test"))).isTrue();
	}

	@Test
	void mustMustNotBeEqualToAnotherFeatureWithDifferentName() {
		Feature feature = new Feature("test", new RoleTerm(new Role("dev")));
		assertThat(feature.equals(new Feature("pouet"))).isFalse();
	}
	
	@Test
	void mustHaveConsistentHashcode() {
		Feature feature = new Feature("test", new RoleTerm(new Role("dev")));
		assertThat(feature.hashCode()).isEqualTo(feature.hashCode());
	}
	
	@Test
	void mustHaveSameHashcodeForIdenticalFeatures() {
		assertThat(new Feature("test", new RoleTerm(new Role("dev"))).hashCode())
		.isEqualTo(new Feature("test").hashCode());
	}
	
	

	static String JSON_WITH_EXPR ="{\"name\":\"My Feature\",\"expression\":{\"@type\":\"term\",\"role\":{\"name\":\"dev\"}}}"; 
	
	static String JSON_WITHOUT_EXPR ="{\"name\":\"My Feature\",\"expression\":null}"; 
	
	@Test
	void mustSerializeWithoutExpr() throws JsonProcessingException  {
		Feature feature = new Feature("My Feature");
		ObjectMapper mapper = new ObjectMapper();
		String actualJson = mapper.writeValueAsString(feature);
		assertThat(actualJson).isEqualTo(JSON_WITHOUT_EXPR);
	}
	
	@Test
	void mustSerializeWithExpr() throws JsonProcessingException  {
		Feature feature = new Feature("My Feature", new RoleTerm(new Role("dev")));
		ObjectMapper mapper = new ObjectMapper();
		String actualJson = mapper.writeValueAsString(feature);
		assertThat(actualJson).isEqualTo(JSON_WITH_EXPR);
	}
	
	
	@Test
	void mustDeserializeWithoutExpr() throws JsonMappingException, JsonProcessingException {
		Feature expected = new Feature("My Feature");
		ObjectMapper mapper = new ObjectMapper();
		Feature actual = mapper.readValue(JSON_WITHOUT_EXPR, Feature.class);
		assertThat(actual).usingRecursiveComparison().isEqualTo(expected);
	}
	
	@Test
	void mustDeserializeWithExpr() throws JsonMappingException, JsonProcessingException {
		Feature expected = new Feature("My Feature", new RoleTerm(new Role("dev")));
		ObjectMapper mapper = new ObjectMapper();
		Feature actual = mapper.readValue(JSON_WITH_EXPR, Feature.class);
		assertThat(actual).isEqualToComparingFieldByFieldRecursively(expected);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
